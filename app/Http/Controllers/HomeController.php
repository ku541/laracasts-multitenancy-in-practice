<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function show()
    {
        return auth()->check() ? view('dashboard') : view('welcome');
    }
}
