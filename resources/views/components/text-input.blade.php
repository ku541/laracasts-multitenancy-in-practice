@props([
    'type' => 'text',
    'label' => '',
    'required' => false,
    'placeholder' => '',
    'autocomplete' => '',
    'maxlength' => 255
])

@php
    $name = $attributes->whereStartsWith('wire:model')->first();
@endphp

<div class="{{ $attributes->get('class') }}">
    <label for="{{ $name }}" class="sr-only">{{ $label }}</label>

    <div class="mt-1 relative rounded-md shadow-sm">
        <input
            {{ $attributes->whereStartsWith('wire:model') }}
            type="{{ $type }}"
            id="{{ $name }}"
            class="appearance-none block border focus:outline-none focus:z-10 px-3 py-2 relative rounded-none rounded-t-md sm:text-sm w-full {{ $errors->has($name) ? ' border-red-300 focus:border-red-500 focus:ring-red-500 placeholder-red-500 text-red-900' : 'border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 placeholder-gray-500 text-gray-900' }}"
            placeholder="{{ $placeholder }}"
            required="{{ $required }}"
            autocomplete="{{ $autocomplete }}"
            maxlength="{{ $maxlength }}"
            @error($name)
                aria-invalid="true"
                aria-describedby="{{ $name }}-error"
            @enderror
        >

        @error($name)
            <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                <svg class="h-5 w-5 text-red-500" fill="currentColor" viewBox="0 0 20 20">
                    <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                </svg>
            </div>
        @enderror
    </div>

    @error($name)
        <p class="mt-2 text-sm text-red-600" id="{{ $name }}-error">{{ $message }}</p>
    @enderror
</div>