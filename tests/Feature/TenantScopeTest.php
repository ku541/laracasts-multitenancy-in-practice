<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\Tenant;
use App\Models\User;

class TenantScopeTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function a_model_has_a_tenant_id_on_the_migration()
    {
        $now = now();

        $this->artisan('make:model Test -m');

        $filename = Str::finish(
            $now->format('Y_m_d_his'),
            '_create_tests_table.php'
        );

        $path = database_path("migrations/$filename");

        $this->assertTrue(File::exists($path));

        $this->assertStringContainsString(
            '$table->foreignIdFor(Tenant::class)->nullable();',
            File::get($path)
        );

        File::delete($path);

        File::delete(app_path('Models/Test.php'));
    }

    /** @test */
    public function a_user_can_only_see_users_in_the_same_tenant()
    {
        $tenants = Tenant::factory()->count(2)->hasUsers(5)->create();

        auth()->login($tenants->first()->users->first());

        $this->assertEquals(5, User::count());
    }

    /** @test */
    public function a_user_can_only_create_a_user_in_their_tenant()
    {
        $tenants = Tenant::factory()->count(2)->hasUsers(5)->create();

        $authenticatedUser = $tenants->first()->users->first();

        auth()->login($authenticatedUser);

        $createdUser = User::factory()->create();

        $this->assertTrue(
            $createdUser->tenant_id === $authenticatedUser->tenant_id
        );
    }

    /** @test */
    public function a_user_can_only_create_a_user_in_their_tenant_when_other_tenants_exist()
    {
        $tenants = Tenant::factory()->count(2)->hasUsers(5)->create();

        $authenticatedUser = $tenants->first()->users->first();

        auth()->login($authenticatedUser);

        $createdUser = User::factory()->create([
            'tenant_id' => $tenants->last()->id
        ]);

        $this->assertTrue(
            $createdUser->tenant_id === $authenticatedUser->tenant_id
        );
    }
}
